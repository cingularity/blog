from flask import Flask, g
from flask_restless import APIManager
from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager, Shell
# import our configuration file
from config import Configuration
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager, current_user
from flask_bcrypt import Bcrypt
#from flask_bootstrap import Bootstrap






app = Flask(__name__)
# Use value from our configuration oject.
app.config.from_object(Configuration)
db = SQLAlchemy(app)
api = APIManager(app, flask_sqlalchemy_db=db)
migrate = Migrate(app, db)

manager = Manager(app)
manager.add_command('db', MigrateCommand)
#bootstrap = Bootstrap(app)
login_manager = LoginManager(app)
login_manager.login_view = 'login'
bcrypt = Bcrypt(app)



@app.before_request
def _before_request():
    g.user = current_user